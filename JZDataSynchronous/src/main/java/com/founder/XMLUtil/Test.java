package com.founder.XMLUtil;

import java.io.PrintWriter;
import java.util.Map;

 import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * @author 作者 E-mail:
 * @date 创建时间：2018年1月26日 上午11:46:54
 * @version 1.0
 * @parameter
 * @since
 * @return
 */
public class Test {
	public static String xmlFile = "xmlfile";

	public static void main(String[] as) throws Exception {

		String root = Thread.currentThread().getContextClassLoader().getResource("").getPath();
		//SqlBean sql = XmlUtil.toBeanFromFile(root, "sql.xml", SqlBean.class);
		String xml = XmlUtil.txt2String( "sql.xml");
		SqlBean sql = XmlUtil.toEntity(xml, SqlBean.class);
		
 
		// toXMLFile(person, root + "/data.xml");
	}

	public static String toXML(Object obj) {
		XStream xstream = new XStream();

		xstream.processAnnotations(obj.getClass()); // 通过注解方式的，一定要有这句话

		return xstream.toXML(obj);
	}

	/*
	 * PrintWriter无追加模式，若写入已有文件，将清空原文件，重新写入；
	 */
	public static void toXMLFile(Object obj, String path) throws Exception {
		XStream xstream = new XStream();

		xstream.processAnnotations(obj.getClass()); // 通过注解方式的，一定要有这句话
		PrintWriter pw = new PrintWriter(path, "utf-8");
		xstream.toXML(obj, pw);
	}
}
