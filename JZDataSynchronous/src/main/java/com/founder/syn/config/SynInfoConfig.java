package com.founder.syn.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.founder.syn.entity.Table;
import com.founder.util.BeanUtil.CopyWhenNullBeanUtilsBean;

/**
 * @author 作者 xc E-mail:
 * @date 创建时间：2018年1月26日 下午2:01:17
 * @version 1.0
 * @parameter
 * @since
 * @return
 */
@Configuration
@Component
@ConfigurationProperties(prefix = "synProps")
public class SynInfoConfig {

	private List<Map<String, String>> tableList = new ArrayList<>(); // 接收prop1里面的属性值

	public List<Map<String, String>> getTableList() {
		return tableList;
	}

	public void setTableList(List<Map<String, String>> tableList) {
		this.tableList = tableList;
	}

	public List<Table> genTable() throws Exception {

		List<Table> list = new ArrayList();
		for (Map<String, String> map : tableList) {
			Table table = new Table();
			new CopyWhenNullBeanUtilsBean().populate(table, map);
			Map<String, String> syn = new LinkedHashMap();
			Map<String, String> replace = new LinkedHashMap();
			Map<String, String> defaultValue = new LinkedHashMap();
			Map<String, String> insert = new LinkedHashMap();
			for (String key : map.keySet()) {
			 
				
				if ( key.startsWith("syn.")) {
				String keySyn=	key.substring("syn.".length());
				syn.put(keySyn, map.get(key));
					 
				}
				else if ( key.startsWith("replace.")) {
					String keyReplace=	key.substring("replace.".length());
					replace.put(keyReplace, map.get(key));
					 
				}
				else if ( key.startsWith("defaultValue.")) {
					String keyDefault=	key.substring("defaultValue.".length());
					defaultValue.put(keyDefault, map.get(key));
				}
				else if ( key.startsWith("insert.")) {
					String keyDefault=	key.substring("insert.".length());
					insert.put(keyDefault, map.get(key));
				}
				else{
					continue;
				}
			}
			table.setDefaultValue(defaultValue);
			table.setReplace(replace);
			table.setSyn(syn);
			table.setInsert(insert);
			list.add(table);
		}
		return list;
	}

}
