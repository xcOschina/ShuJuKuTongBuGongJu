package com.founder.syn.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;

import com.founder.syn.entity.Table;
import com.founder.syn.log.LoggerUtil;

/**
 * @author 作者 xc E-mail:
 * @date 创建时间：2018年1月30日 上午11:10:52
 * @version 1.0
 * @parameter
 * @since
 * @return
 */
@Configuration
@Aspect
public class LogAop extends LoggerUtil{

	@Pointcut("execution(* com.founder.syn.dao.*.*(..))")
	public void executeService() {

	}

	/**
	 * 后置异常通知 定义一个名字，该名字用于匹配通知实现方法的一个参数名，当目标方法抛出异常返回后，将把目标方法抛出的异常传给通知方法；
	 * throwing 限定了只有目标方法抛出的异常与通知方法相应参数异常类型时才能执行后置异常通知，否则不执行，
	 * 对于throwing对应的通知方法参数为Throwable类型将匹配任何异常。
	 * 
	 * @param joinPoint
	 * @param exception
	 */
	@AfterThrowing(value = "executeService()", throwing = "exception")
	public void doAfterThrowingAdvice(JoinPoint joinPoint, Throwable exception) {
		// 目标方法名：
		// 获取目标方法的参数信息
		Object[] obj = joinPoint.getArgs();
		Table table= (Table) obj[0];
		Object target = joinPoint.getTarget();
  		String owner = table.getOwner();
 		String outer = table.getOuter();
 		StringBuffer sb = new StringBuffer();
 		sb.append(" owner表 : ");
 		sb.append(owner);
 		sb.append(" 和——outer表 :");
 		sb.append(outer);
 		sb.append(" 进行同步执行——  ");
 		sb.append(joinPoint.getSignature().getName());
 		sb.append(" ：出现了异常");
 		LOG.error(sb.toString());
		 
	}
}
