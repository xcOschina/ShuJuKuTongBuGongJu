package com.founder.syn.entity;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author 作者 xc E-mail:
 * @date 创建时间：2018年1月26日 下午2:06:05
 * @version 1.0
 * @parameter
 * @since
 * @return
 */
 
public class Table {

	/*
	 * old表
	 */
	private String owner;

	/*
	 * 新表
	 */
	private String outer;

	/*
	 * 方法 可自定义扩展
	 */
	private String method;

	/*
	 * 需要同步的字段
	 */
	private Map<String, String> syn;
	

	/*
	 * 需要添加的字段
	 */
	private Map<String, String> insert;

	/*
	 * 需要替换的字段
	 */
	private Map<String, String> replace;

	/*
	 * 默认的值
	 */
	private Map<String, String> defaultValue;

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getOuter() {
		return outer;
	}

	public void setOuter(String outer) {
		this.outer = outer;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Map<String, String> getSyn() {
		return syn;
	}

	public void setSyn(Map<String, String> syn) {
		this.syn = syn;
	}

	public Map<String, String> getReplace() {
		return replace;
	}

	public void setReplace(Map<String, String> replace) {
		this.replace = replace;
	}

	public Map<String, String> getDefaultValue() {
		
		return defaultValue;
	}

	public void setDefaultValue(Map<String, String> defaultValue) {
		for (Map.Entry<String, String> entry : defaultValue.entrySet()) {
			 
			StringBuffer sb = new StringBuffer();
			sb.append("'");
			sb.append( entry.getValue());
			sb.append("'");
			entry.setValue(sb.toString());
			 
		}
		
		this.defaultValue = defaultValue;
	}

	public Map<String, String> getInsert() {
		return insert;
	}

	public void setInsert(Map<String, String> insert) {
		this.insert = insert;
	}

	

}
