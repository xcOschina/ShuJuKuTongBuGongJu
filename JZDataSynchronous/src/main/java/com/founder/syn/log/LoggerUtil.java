package com.founder.syn.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.founder.syn.main.SynTableListProcessor;

/**
 * @author 作者 E-mail:
 * @date 创建时间：2018年1月30日 下午1:22:48
 * @version 1.0
 * @parameter
 * @since
 * @return
 */
public class LoggerUtil {
	public final Logger LOG = LoggerFactory.getLogger(this.getClass());
}
