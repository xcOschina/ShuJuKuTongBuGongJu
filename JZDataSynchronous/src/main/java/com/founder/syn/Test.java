package com.founder.syn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import com.founder.syn.config.SynInfoConfig;

/** 
* @author  作者 E-mail: 
* @date 创建时间：2018年1月26日 下午2:15:44 
* @version 1.0 
* @parameter  
* @since  
* @return  
*/
@Component
@EnableConfigurationProperties({SynInfoConfig.class})
public class Test {

	@Autowired
	private SynInfoConfig synInfoConfig;
	
	public void test(){
		System.out.println(synInfoConfig );
	}
}
