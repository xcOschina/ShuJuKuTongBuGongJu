package com.founder.syn.scheduled;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.founder.XMLUtil.XmlUtil;
import com.founder.syn.check.SqlCheck;
import com.founder.syn.service.SynService;

/**
 * @author 作者 E-mail:
 * @date 创建时间：2018年1月31日 上午10:20:38
 * @version 1.0
 * @parameter
 * @since
 * @return
 */
@Component
@Configuration
public class MyTask {
	@Value("${application.db.check}")
	private String[] check;

	@Autowired
	private SynService synService;

	@Autowired
	private SqlCheck sqlCheck;

	@Test
	public void test(){
		String sqls= " ;a ;b ; c; ";
		sqls=sqls.trim();
		String[] sqlsArr =sqls.split(";");
		for(String s:sqlsArr ){
			if(s.equals("")){
				System.out.println(234);
			}
			System.out.println(s);
		}
	}
	
	public void task() throws Exception {
		for (String path : check) {

			String sqls = XmlUtil.txt2String(path);
			sqls = sqls.trim();
			String[] sqlsArr =sqls.split(";");
			for(String sql : sqlsArr){
				if(sql.equals("")){
					continue;
				}
				sqlCheck.sql(sql);
			}
			 
		}
		synService.test();
	}
}
