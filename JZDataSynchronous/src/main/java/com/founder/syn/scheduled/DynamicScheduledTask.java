package com.founder.syn.scheduled;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.concurrent.ScheduledFuture;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * @author 作者 E-mail:
 * @date 创建时间：2018年1月31日 上午10:01:42
 * @version 1.0
 * @parameter
 * @since
 * @return
 */
@Component
public class DynamicScheduledTask {

	@Value("${application.task.taskMethod}")
	private String taskMethod;

	@Value("${application.task.cronStr}")
	private String cronStr;

	@Value("${application.task.task}")
	private String task;

	@Resource
	private MyTask myTask;

	@Autowired
	private ThreadPoolTaskScheduler threadPoolTaskScheduler;

	private ScheduledFuture<?> future;

	@Bean
	public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
		return new ThreadPoolTaskScheduler();
	}

	public String startCron2() {
		future = threadPoolTaskScheduler.schedule(new MyRunnable(), new CronTrigger("0/5 * * * * *"));
		System.out.println("DynamicTask.startCron()");
		return "startCron";
	}

	public String stop() {
		if (future != null) {
			future.cancel(true);
		}
		System.out.println("DynamicTask.stopCron()");
		return "stopCron";
	}

	public String start() {
		stop();// 先停止，在开启.
		future = threadPoolTaskScheduler.schedule(new MyRunnable(), new Trigger() {

			public Date nextExecutionTime(TriggerContext triggerContext) {
				return new CronTrigger(cronStr).nextExecutionTime(triggerContext);
			}
		});
		System.out.println("DynamicTask.startCron()");
		return "changeCron10";
	}

	private class MyRunnable implements Runnable {

		public void run() {
			/*
			 * Class clazz = synDao.getClass(); Method m = clazz.getMethod(s,
			 * t.getClass()); m.invoke(synDao, t);
			 */
			Class clazz = myTask.getClass();
			try {
				Method m = clazz.getMethod(taskMethod);
				m.invoke(myTask);
			} catch (NoSuchMethodException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}
		}
	}

	public void task() {
		if ("start".equals(task)) {
			start();
		}
		if ("stop".equals(task)) {
			stop();
		}
	}

}
