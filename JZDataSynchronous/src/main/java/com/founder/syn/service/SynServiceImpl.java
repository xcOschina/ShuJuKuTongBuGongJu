package com.founder.syn.service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.founder.syn.config.SynInfoConfig;
import com.founder.syn.dao.SynDao;
import com.founder.syn.entity.Table;
import com.founder.syn.log.LoggerUtil;

/**
 * @author 作者 E-mail:
 * @date 创建时间：2018年1月29日 下午2:16:40
 * @version 1.0
 * @parameter
 * @since
 * @return
 */
@Service
public class SynServiceImpl extends LoggerUtil implements SynService {

	@Autowired
	private SynDao synDao;

	@Autowired
	private SynInfoConfig synInfoConfig;

	public void test() throws Exception {
		List<Table> list = new ArrayList();
		try {
			list = synInfoConfig.genTable();
		} catch (Exception e) {
			LOG.error("配置信息数据错误");
		}
		for (Table t : list) {

			int i = synDao.select(t);
			if (i == 0) {
				if (t.getMethod().contains("replace")) {
					synTable(t);
				}
			} else {
				synTable(t);
			}

		}
	}

	public void synTable(Table t) throws Exception {
		String method = t.getMethod();
		String[] sarr = method.split("-");
		for (String s : sarr) {
			Class clazz = synDao.getClass();
			Method m = clazz.getMethod(s, t.getClass());
			m.invoke(synDao, t);

		}
	}

}
