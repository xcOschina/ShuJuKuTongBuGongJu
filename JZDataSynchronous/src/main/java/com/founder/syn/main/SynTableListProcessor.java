package com.founder.syn.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.founder.XMLUtil.XmlUtil;
import com.founder.syn.check.SqlCheck;
import com.founder.syn.log.LoggerUtil;
import com.founder.syn.scheduled.DynamicScheduledTask;
import com.founder.syn.service.SynService;

/**
 * @author 作者 xc E-mail:
 * @date 创建时间：2018年1月26日 下午2:01:17
 * @version 1.0
 * @parameter
 * @since spring上下文初始化完成后-执行同步工具
 * @return
 */
@Component
public class SynTableListProcessor extends LoggerUtil implements ApplicationListener<ContextRefreshedEvent> {

	@Value("${application.db.check}")
	private String[] check;

	@Autowired
	private SynService synService;

	@Autowired
	private SqlCheck sqlCheck;

	@Autowired
	private DynamicScheduledTask dynamicScheduledTask;

	public void onApplicationEvent(ContextRefreshedEvent event) {

		try {
			for (String path : check) {
				String sqls = XmlUtil.txt2String(path);
				sqls = sqls.trim();
				String[] sqlsArr =sqls.split(";");
				for(String sql : sqlsArr){
					if(sql.equals("")){
						continue;
					}
					sqlCheck.sql(sql);
				}
			}
			synService.test();
			dynamicScheduledTask.task();
		} catch (Exception e) {
			LOG.error("同步错误", e);
			return;
		}
		LOG.info("数据同步成功");
	}
}