package com.founder.util.BeanUtil;

import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.PropertyUtils;
/*
 * 如果dest中某字段有值，则该字段不复制；
 */
public class CopyWhenNullBeanUtilsBean extends BeanUtilsBean{  
	 @Override  
	    public void copyProperty(Object bean, String name, Object value)  
	            throws IllegalAccessException, InvocationTargetException {  
	        try {  
	            Object destValue = PropertyUtils.getSimpleProperty(bean, name);  
	            if (destValue == null) {  
	                super.copyProperty(bean, name, value);  
	            }  
	        } catch (NoSuchMethodException e) {  
	            throw new RuntimeException(e);  
	        }  
	    }  
	 
	 
	  public void populate(Object bean, Map properties)
		        throws IllegalAccessException, InvocationTargetException {

		        // Do nothing unless both arguments have been specified
		        if ((bean == null) || (properties == null)) {
		            return;
		        }
		      

		        // Loop through the property name/value pairs to be set
		        Iterator entries = properties.entrySet().iterator();
		        while (entries.hasNext()) {

		            // Identify the property name and value(s) to be assigned
		            Map.Entry entry = (Map.Entry)entries.next();
		            String name = (String) entry.getKey();
		            if (name == null) {
		                continue;
		            }

		            try{
		            // Perform the assignment for this property
		            setProperty(bean, name, entry.getValue());
		            }catch (Exception e ){
		            	
		            }

		        }

		    }
  
}  
