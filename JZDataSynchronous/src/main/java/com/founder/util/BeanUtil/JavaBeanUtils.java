package com.founder.util.BeanUtil;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.BooleanConverter;
import org.apache.commons.beanutils.converters.ByteConverter;
import org.apache.commons.beanutils.converters.DateConverter;
import org.apache.commons.beanutils.converters.DoubleConverter;
import org.apache.commons.beanutils.converters.FloatConverter;
import org.apache.commons.beanutils.converters.IntegerConverter;
import org.apache.commons.beanutils.converters.LongConverter;
import org.apache.commons.beanutils.converters.ShortConverter;

 

public class JavaBeanUtils {

	/*
	 * 1.8.3 以下版本 的方法(包括1.8。3)
	 */
	JavaBeanUtils(Object obj) {
		oldBeanUtil();
	}

	public static Map<String, String> describe(Object bean) throws Exception {
		return BeanUtils.describe(bean);
	}

	public static void populate(Object bean, Map map) throws Exception {
		BeanUtils.populate(bean, map);
	}

	private static void oldBeanUtil() {
		ConvertUtils.register(new IntegerConverter(null), Integer.class);
		ConvertUtils.register(new DoubleConverter(null), Double.class);
		ConvertUtils.register(new ByteConverter(null), Byte.class);
		ConvertUtils.register(new FloatConverter(null), Float.class);
		ConvertUtils.register(new LongConverter(null), Long.class);
		ConvertUtils.register(new BooleanConverter(null), Boolean.class);
		ConvertUtils.register(new ShortConverter(null), Short.class);
		ConvertUtils.register(new DateConverter(null), java.util.Date.class);
		ConvertUtils.register(new org.apache.commons.beanutils.converters.BigDecimalConverter(null), BigDecimal.class);

		ConvertUtils.register(new org.apache.commons.beanutils.converters.SqlTimestampConverter(null),
				java.sql.Timestamp.class);
		ConvertUtils.register(new org.apache.commons.beanutils.converters.SqlDateConverter(null), java.sql.Date.class);
		ConvertUtils.register(new org.apache.commons.beanutils.converters.SqlTimeConverter(null), java.sql.Time.class);
	}

	public static void main(String[] ags) throws Exception, Exception {
	 
	 
		 Map<String, String> map = new HashMap<String, String>();
		  map.put("1", "value1");
		  map.put("2", "value2");
		  map.put("3", "value3");
		  Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
		  while (it.hasNext()) {
		   Map.Entry<String, String> entry = it.next();
		   System.out.println("key= " + entry.getKey() + " and value= " + entry.getValue());
		  }

	}

	/*
	 * 属性完全复制
	 */
	public static void copyProperties(Object dest, Object orig) throws Exception {
		BeanUtils.copyProperties(dest, orig);
	}

	/*
	 * 属性空的才会复制
	 */
	public static void nullCopyProperties(Object dest, Object orig) throws Exception {
		new CopyWhenNullBeanUtilsBean().copyProperties(dest, orig);
	}

	/*
	 * 只复制不为空的属性
	 */
	public static void copyNotNullProperties(Object dest, Object orig) throws Exception {

		new CopyNotNullBeanUtilsBean().copyProperties(dest, orig);
	}

}
