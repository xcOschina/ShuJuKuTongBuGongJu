package com.founder.util.BeanUtil;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtilsBean;

/*
 * 如果orig中某字段没值（为null），则该字段不复制，
 */
public class CopyNotNullBeanUtilsBean extends BeanUtilsBean {  
  
    @Override  
    public void copyProperty(Object bean, String name, Object value) throws IllegalAccessException, InvocationTargetException {  
        if (value == null) {  
            return;  
        }  
        super.copyProperty(bean, name, value);  
        
        
    }  
}  
