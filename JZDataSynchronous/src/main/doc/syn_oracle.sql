/*
 Navicat Premium Data Transfer

 Source Server         : 10.20.20.2_1521_fzbd
 Source Server Type    : Oracle
 Source Server Version : 100200
 Source Host           : 10.20.20.2:1521
 Source Schema         : BOZHOUGC

 Target Server Type    : Oracle
 Target Server Version : 100200
 File Encoding         : 65001

 Date: 15/03/2018 16:12:49
*/


-- ----------------------------
-- Table structure for T_OUTER
-- ----------------------------
DROP TABLE "BOZHOUGC"."T_OUTER";
CREATE TABLE "BOZHOUGC"."T_OUTER" (
  "USERID" VARCHAR2(50 BYTE) ,
  "ACCOUNT" VARCHAR2(50 BYTE) ,
  "PASSWORD" VARCHAR2(50 BYTE) 
)
TABLESPACE "BOZHOUGC"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of "T_OUTER"
-- ----------------------------
INSERT INTO "BOZHOUGC"."T_OUTER" VALUES ('2', '12', '1');
INSERT INTO "BOZHOUGC"."T_OUTER" VALUES ('1', '12', '11');
INSERT INTO "BOZHOUGC"."T_OUTER" VALUES ('1', '11', '111');

-- ----------------------------
-- Table structure for T_TEST
-- ----------------------------
DROP TABLE "BOZHOUGC"."T_TEST";
CREATE TABLE "BOZHOUGC"."T_TEST" (
  "USERID" VARCHAR2(20 BYTE) ,
  "USERNAME" VARCHAR2(50 BYTE) ,
  "PASSWORD" VARCHAR2(50 BYTE) 
)
TABLESPACE "BOZHOUGC"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of "T_TEST"
-- ----------------------------
INSERT INTO "BOZHOUGC"."T_TEST" VALUES ('2', '12', '1');
INSERT INTO "BOZHOUGC"."T_TEST" VALUES ('1', '12', '11');
INSERT INTO "BOZHOUGC"."T_TEST" VALUES ('1', '12', '11');
INSERT INTO "BOZHOUGC"."T_TEST" VALUES ('1', '11', '111');
